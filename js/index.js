var FormLevels = function ({el, stepEl, nextButton, previousButton, hiddenStyle}) {
  this.el = el
  this.forms = el.querySelectorAll(stepEl)
  this.formsMemory = {}
  this.hiddenStyle = hiddenStyle

  this.nextButtons = el.querySelectorAll(`${stepEl} ${nextButton}`)

  for (let i = 0; i < this.nextButtons.length; i++) {
    this.nextButtons[i].onclick = partial(this.nextForm, [i], this)
  }

  this.previousButtons = el.querySelectorAll(`${stepEl} ${previousButton}`)

  for (let i = 0; i < this.nextButtons.length; i++) {
    this.previousButtons[i].onclick = partial(this.previousForm, [i], this)
  }

  this.goToForm(0)
}

function partial (fn, args, context = null) {
  let slice = Array.prototype.slice

  return function() {
    return fn.apply(context, args.concat(slice.call(arguments, 0)));
  }
}

FormLevels.prototype.goToForm = function (formId) {
  for (let i = 0; i < this.forms.length; i++) {
    if (i !== formId) {
      this.forms[i].style = this.hiddenStyle
    } else {
      this.forms[i].style = ''
    }
  }
}

FormLevels.prototype.nextForm = function (current) {
  this.rememberForm(current)
  for (let i = 0; i < this.forms.length; i++) {
    if (i !== current + 1) {
      this.forms[i].style = this.hiddenStyle
    } else {
      this.forms[i].style = ''
    }
  }
}

FormLevels.prototype.previousForm = function (current) {
  for (let i = 0; i < this.forms.length; i++) {
    if (i !== current - 1) {
      this.forms[i].style = this.hiddenStyle
    } else {
      this.forms[i].style = ''
    }
  }
}

FormLevels.prototype.rememberForm = function (formId) {}

let _ = new FormLevels({
  el: document.getElementById('myform'),
  stepEl: '.step',
  nextButton: '.next',
  previousButton: '.prev',
  hiddenStyle: 'display: none;'
})
